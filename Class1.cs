﻿using System;
using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public class Sequence : ISequencePattern
    {
        public List<IPlainPattern> PatternList { get; set; }

        public string Name { get { return "Последовательность"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            ExactMatch ex = new ExactMatch();
            bool result = false;
            for (int i = 0; !result && i < basicAnswers.Count; i++)
                result = ex.IsAnswerCorrect(studentAnswers, basicAnswers);
            return result;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class ExactMatch : IPlainPattern
    {
        public string Name { get { return "Точное совпадение"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            string basicAnswer = basicAnswers[0];
            string answer = studentAnswers[0];
            return string.Equals(basicAnswer.Trim(), answer.Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}